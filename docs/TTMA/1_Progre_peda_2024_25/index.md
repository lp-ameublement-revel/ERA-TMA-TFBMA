---
author: Alègre André
title: Progression pédagogique 2024/25
---


## Réforme BAC Pro 2024/2025

!!! warning "Une terminale pro adaptée au projet de l'élève"

    La terminale pro comprendra un tronc commun et, au choix, deux parcours (6 semaines de stage pour faciliter l'insertion vers l'emploi après un bac pro ou 6 semaines de cours renforcés pour préparer l'entrée dans l'enseignement supérieur). Les enseignements en co-intervention disparaissent de l'emploi du temps des terminales pro. Les épreuves du baccalauréat professionnel débuteront au mois de mai.

