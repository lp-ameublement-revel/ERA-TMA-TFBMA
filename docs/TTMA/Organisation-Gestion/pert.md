---
author: Alègre André
title: Le PERT
---
**S9  L’organisation et la gestion de fabrication et de chantier** <br>
    9.12 - La chronologie des étapes

!!! info "**Définition**"
    Le PERT : Program Evaluation and Rewiew Technique <br>
    Ou en français Technique d’élaboration et de contrôle d’un programme. <br><br>
 
    Le PERT est une méthode d’organisation consistant à mettre en ordre,<br> 
    sous forme de réseaux, les opérations (appelées aussi tâches) suivant leur 
    antériorité et leur durée. <br><br>