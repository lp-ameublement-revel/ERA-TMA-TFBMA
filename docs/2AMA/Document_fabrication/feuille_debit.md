---
author: Alègre André
title: Feuille de débit
---
<center>
![Logo LP revel](../images/logo-revel.png)
</center>

**S2 La communication technique** <br>
    S2.2 - Les documents techniques<br>

**Les objectifs :** <br>
- Remplir une fiche de débit.<br>
- Calculer le brut à partir des dimensions finis.<br>
- Calculer le M3 (métre  cube) ou le M2 (métre carré : surface) des différents matériaux.<br>
- Faire un devis.<br>

!!! info " 1 - Définition :"
    La feuille de débit (ou fiche de débit) est un élément essentiel lors de la fabrication d'un ouvrage. <br>
    C'est une liste de toutes les pièces à débiter de  l'ouvrage (meuble, cuisine ect....).<br>
    Nous trouverons dans cette liste:<br>
    1. la désignation des pièces et leur nombre.<br>
    2.  Longueur, largeur, épaisseur des pièces en **_ mm (milimètre) _**. Ainsi que la Longueur brute, largeur brute et épaisseur commerciale si c'est du bois massif.<br>

!!! example "Exemple"
    **Prenons le cadre en bois ci-dessous.**<br>
    La Longueur est dans le sens du fil du bois. <br>
    Nous avons 2 traverses de Longueur = 400 mm, largeur =40 mm et épaisseur = 28 mm <br>
               2 montants de Longueur = 600 mm, largeur =40 mm et épaisseur = 28 mm <br>

![Cadre bois](../images/cadre-2.png)

!!! example "Exemple :"   
    Comme les traverses en haut et en bas sont les mêmes, nous n'avons pas besoin de les nonmer différement (traverse haute et traverse basse).<br>
    Idem pour les montants.<br>

Voici la feuille de débit simplifiée avec les dimensions finies.


   | Nombre | Désignation | Longueur | largeur | épaisseur |
:---:|:---:|:---:|:---:|:---:|
| 2      | Traverse   | 400      | 40      | 28        |
| 2      | Montant    | 600      | 40      | 28        |

<br>

## 3 - Dimensions finies et dimensions brutes:  
 
 Le débit et le corroyage entraînant des pertes en matière; il est nécessaire de prévoir une surcote.<br>

 Généralement on rajoute aux dimensions finies:<br>
 Longueur finie + 30 mm = Longueur brute . largeur finie + 7 mm = largeur brute<br>
 Longueur finie = Épaisseur commerciale: les épaisseurs des planches sèches sortant de scierie: 18 mm, 27 mm, 34 mm, 45 mm, 54mm, 65mm ......<br>

!!! warning "Remarque" 

     Attention les surcotes peuvent augmenter si vous avez des grandes longueur à débiter ou que vôtre bois est déformé, ou avec des défauts.

<br>
 Reprenons la feuille de débit. <br>
<style>
    table { border-collapse:collapse; border-spacing:0; empty-cells:show }
    td, th { vertical-align:top; font-size:12pt;}
    h1, h2, h3, h4, h5, h6 { clear:both;}
    ol, ul { margin:0; padding:0;}
    li { list-style: none; margin:0; padding:0;}
    span.footnodeNumber { padding-right:1em; }
    span.annotation_style_by_filter { font-size:95%; font-family:Arial; background-color:#fff000;  margin:0; border:0; padding:0;  }
    span.heading_numbering { margin-right: 0.8rem; }* { margin:0;}
    .P1 { font-size:12pt; font-family:Arial; writing-mode:horizontal-tb; direction:ltr;text-align:center ! important; }
    .P2 { font-size:12pt; font-family:Arial; writing-mode:horizontal-tb; direction:ltr;text-align:center ! important; font-weight:bold; }
    .Standard { font-size:12pt; font-family:Arial; writing-mode:horizontal-tb; direction:ltr;}
    .Text_20_body { font-size:12pt; font-family:Arial; writing-mode:horizontal-tb; direction:ltr;margin-top:0cm; margin-bottom:0.247cm; line-height:115%; }
    .Tableau1 { width:17.50cm; float:none; }
    .Tableau1_A1 { padding:0.097cm; border-left-width:NaNcm; border-left-style:solid; border-left-color:#000000; border-right-style:none; border-top-width:NaNcm; border-top-style:solid; border-top-color:#000000; border-bottom-width:NaNcm; border-bottom-style:solid; border-bottom-color:#000000; }
    .Tableau1_A2 { padding:0.097cm; border-left-width:NaNcm; border-left-style:solid; border-left-color:#000000; border-right-style:none; border-top-style:none; border-bottom-width:NaNcm; border-bottom-style:solid; border-bottom-color:#000000; }
    .Tableau1_F1 { border-top-width:NaNcm; border-top-style:solid; border-top-color:#000000; border-left-width:NaNcm; border-left-style:solid; border-left-color:#000000; border-bottom-width:NaNcm; border-bottom-style:solid; border-bottom-color:#000000; border-right-width:NaNcm; border-right-style:solid; border-right-color:#000000; padding:0.097cm; }
    .Tableau1_H2 { padding:0.097cm; border-left-width:NaNcm; border-left-style:solid; border-left-color:#000000; border-right-width:NaNcm; border-right-style:solid; border-right-color:#000000; border-top-style:none; border-bottom-width:NaNcm; border-bottom-style:solid; border-bottom-color:#000000; }
    .Tableau1_A { width:0.993cm; }
    .Tableau1_B { width:3.256cm; }
    .Tableau1_C { width:2.125cm; }
    .Tableau1_E { width:2.124cm; }
    /* ODF styles with no properties representable as CSS:
     { } */
</style>
	
<table border="0" cellspacing="0" cellpadding="0" class="Tableau1"><colgroup>
	<col width="43"/><col width="142"/><col width="93"/><col width="93"/><col width="93"/><col width="93"/><col width="93"/><col width="93"/></colgroup>
<tr><td rowspan="2" style="text-align:left;width:0.993cm; " class="Tableau1_A1"><p class="P2"> </p><p class="P2">Nb</p></td>
    <td rowspan="2" style="text-align:left;width:3.256cm; " class="Tableau1_A1"><p class="P2"> </p><p class="P2">Désignation</p></td>
    <td colspan="3" style="text-align:left;width:2.125cm; " class="Tableau1_A1"><p class="P2">Dimensions finies</p></td>
    <td colspan="3" style="text-align:left;width:2.125cm; " class="Tableau1_F1"><p class="P2">Dimensions brutes</p></td>
</tr>
<tr><td style="text-align:left;width:2.125cm; " class="Tableau1_A2"><p class="P2">Lon</p></td>
    <td style="text-align:left;width:2.125cm; " class="Tableau1_A2"><p class="P2">larg</p></td>
    <td style="text-align:left;width:2.124cm; " class="Tableau1_A2"><p class="P2">Ep</p></td>
    <td style="text-align:left;width:2.200cm; " class="Tableau1_A2"><p class="P2">Lon + 30</p></td>
    <td style="text-align:left;width:2.125cm; " class="Tableau1_A2"><p class="P2">lar + 7</p></td>
    <td style="text-align:left;width:3cm; " class="Tableau1_H2"><p class="P2">Ep commerciale</p></td></tr>
<tr><td style="text-align:left;width:0.993cm; " class="Tableau1_A2"><p class="P1">2</p></td>
    <td style="text-align:left;width:3.256cm; " class="Tableau1_A2"><p class="P1">Traverse</p></td>
    <td style="text-align:left;width:2.125cm; " class="Tableau1_A2"><p class="P1">400</p></td>
    <td style="text-align:left;width:2.125cm; " class="Tableau1_A2"><p class="P1">40</p></td>
    <td style="text-align:left;width:2.124cm; " class="Tableau1_A2"><p class="P1">28</p></td>
    <td style="text-align:left;width:2.125cm; " class="Tableau1_A2"><p class="P1">430</p></td>
    <td style="text-align:left;width:2.125cm; " class="Tableau1_A2"><p class="P1">47</p></td>
    <td style="text-align:left;width:2.125cm; " class="Tableau1_H2"><p class="P1">34</p></td>
</tr>
<tr><td style="text-align:left;width:0.993cm; " class="Tableau1_A2"><p class="P1">2</p></td>
    <td style="text-align:left;width:3.256cm; " class="Tableau1_A2"><p class="P1">Montant</p></td>
    <td style="text-align:left;width:2.125cm; " class="Tableau1_A2"><p class="P1">600</p></td>
    <td style="text-align:left;width:2.125cm; " class="Tableau1_A2"><p class="P1">40</p></td>
    <td style="text-align:left;width:2.124cm; " class="Tableau1_A2"><p class="P1">28</p></td>
    <td style="text-align:left;width:2.125cm; " class="Tableau1_A2"><p class="P1">630</p></td>
    <td style="text-align:left;width:2.125cm; " class="Tableau1_A2"><p class="P1">47</p></td>
    <td style="text-align:left;width:2.125cm; " class="Tableau1_H2"><p class="P1">34</p></td>
</tr></table>
<br>
Cette feuille de débit nous permet aussi de calculer le m² pour les panneaux et le m³ pour le bois.<br>
Il faut d'abord convertir toutes nos mesures en mètre . <br>

| m | dm | cm | mm |
|:---:|:---:|:---:|:---:|
|  0     | 4   | 0      | 0     | 

!!! warning "Remarque"
     On lit sur ce tableau q'une longueur de 400 mm = 0.4 m <br> 
     Ce qui est équivalent à diviser 400 par 1000.<br>
     400 ÷ 1000 = 0.4<br>

Calculons le m³ necessaire pour fabriquer le cadre en bois pour photo (de notre exple)<br>
$2 \times (430 \div 1000)\times(47 \div 1000)\times(34 \div 1000)=0,00137$<br>
$2 \times (630 \div 1000)\times(47 \div 1000)\times(34 \div 1000)=0,00201$<br>
$0.00137 + 0.00201 =0.00338$ m³ <br>



## 4 - Format d'une feuille de débit:  


![Feuille débit](../images/format-feuille-debit.png){ width=70% }




## 4 - Télécharger la feuille de débit en fichier .calc:  
 
!!! warning "Remarque" 

      Pour utiliser cette feuille de calcul ne changer pas, les cellules avec du texte rouge; car elles ont des formules <br>
    

      


