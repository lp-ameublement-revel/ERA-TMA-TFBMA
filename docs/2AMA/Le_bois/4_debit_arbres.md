---
author: Alègre André
title: Débit des troncs d'arbres
---
!!! info "Définition"
    Les différents débits conduisent à la production d’éléments (avivés, équarris, plateaux, placages…) avec des caractéristiques spécifiques.<br>
    En effet, une pièce de bois présente un aspect et des propriétés particulières selon l’orientation de sa plus grande face par rapport aux directions<br>
    longitudinale, radiale ou tangentielle du bois ; on dit alors que la pièce de bois présente un « type de débit ». Les 3 principaux sont les suivants :<br>


| Type de débit des avivés | Orientation de sa plus grande face | Tolérance angulaire |
:---:|:---:|:---:|
| Quartier    |Parallèle à la direction radiale  | 0° à 30°  |
| Dosse     |Perpendiculaire à la direction radiale   |70° à 90°    |
| Faux-quartier    | En position intermédiaire entre dosse et quartier   | 30° à 70°     |


![arbre](../images/different-debit.jpg)