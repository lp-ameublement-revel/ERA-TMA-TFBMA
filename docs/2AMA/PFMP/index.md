---
author: Alègre André
title: PFMP
---

!!! danger "Pèriode PFMP 2024-2025"
    **Première période:** 27 janvier 2025 au 14 février 2025 <br>
    **Deuxième période:** 10 mai 2025 au 31 mai 2025 <br>


    Calendrier PFMP:[Ouvrir dans un nouvel onglet](../../a_telecharger/periode-PFMP.pdf){:target="_blank" }


!!! info "Voici une carte interactive des entreprises qui ont acceuillit des stagiaires "

    [Carte interactive PFMP](https://umap.openstreetmap.fr/fr/map/era-tma-tfbma_1180574?scaleControl=false&miniMap=false&scrollWheelZoom=true&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=null&onLoadPanel=none&captionBar=false&captionMenus=true#11/43.7334/1.7867){:target="_blank" }


!!! info "Rapport de stage"
    Aprés chaque pèriode de stage (PFMP), nous demandons un rapport d’activités conduites en entreprise.<br>
    Que ce soit en seconde, première et terminale Bac Pro.<br>
    En Terminale il y a une Sous-épreuve E.31 : Réalisation et suivi des ouvrages en entreprise (coefficient 2)<br>



