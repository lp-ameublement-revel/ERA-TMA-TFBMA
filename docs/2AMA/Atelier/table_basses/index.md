---
author: Alègre André
title: Table basse
---

!!! note "Table basse en hêtre et CP cintrable."
     
      <div class="centre">
      <iframe 
      src="../a_telecharger/table-basse-plan.pdf"
      width="1000" height="1000" 
      frameborder="0" 
      allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
      </iframe>
      </div>

