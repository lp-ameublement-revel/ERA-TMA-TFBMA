
**S5.2 L'étude des ouvrages** <br>
    S5.21 - Les familles d'ouvrages.<br>
    
**Les objectifs :** <br>
- Apprende le vocabulaire <br>
- Calcul de l'ouvrage ici l'escalier <br>
- Connaître la réglementation Française.<br>
- Dessiner l'escalier en vue de dessus.<br>


## 1 - Le vocabulaire : <br><br>
  <b>Définition:</b> Un escalier est une suite régulière de marches, ou degrés qui servent à monter ou à descendre. Il permet ainsi de passer d'un niveau à un autre. C'est un lien indispensable entre des étages différents.



<img
  src="../img/Hauteur-Pas.jpg"
  alt="The head and torso of a dinosaur skeleton;
          it has a large head with long sharp teeth"
  width="400"
  height="341" />


 <span style="color: #FF0000">**H**= Hauteur de marche <br>
  **G**= Giron </span>


<img
  src="../img/Escalier-vue-Face.jpg"
  alt="The head and torso of a dinosaur skeleton;
          it has a large head with long sharp teeth"
  width="800"
  height="682" />

<span style="color: #FF0000">La reculée  ou la ligne de foulée est  la longueur de l escalier<br>


<img
  src="../img/Escalier-vue-Dessus.jpg"
  alt="The head and torso of a dinosaur skeleton;
          it has a large head with long sharp teeth"
  width="800"
  height="681" />

## 2 - Calcul d'un escalier : 

Le "module " est la règle de base au calcul d'un escalier. Elle est le fruit de la réflexion de Nicolas-François Blondel (1618-1686) mathématicien ingénieur.
  </br>

!!! info "La formule de Blondel:"
    M = 2H + G (Module = 2 hauteurs + 1 giron). <br>
    Pour que l 'escalier soit confortable le résultat doit être supérieur à 600 et inférieur à 640 mm</br>
    [600< 2H + G>640](#){.btn .btn-info}


   Prenons pas exemple un escalier de hauteur à franchir 2950 et de reculée 4200<br>

Sachant que la hauteur de marche  est comprise entre **160 mm < H > 180 mm**. </br>

On calcule le nombre de marches, en divisant la hauteur à franchir par la hauteur de marche ( 170mm) <br>
2950÷170= 17,35 <br>
On arrondi et donc on aura 17 marches. La hauteur exacte des marches est 2950÷17=173,53 <br>
Le giron est égale à la reculée diviser par le nombre de marches - 1 car la dernière ne compte pas car c'est le palier . <br>
 4200÷(17-1)=262,5 . <br>

On applique la formule de Blondel  Module = (2x173,53)+ 262,5 = 609,56. on est bien compris entre 600< 609 >640 <br><br>

[Télécharger le vocabulaire de l'escalier](Pdf/escalier-Vocabulaire.pdf){ .md-button target="_blank" rel="noopener" }


!!!info "**3 - Tracer de l'escalier :**" 
    Méthode de la division par cercle pour tracer le balancement des marches d'un escalier 1/4 tournant.<br>
    Exemple d'un escalier ci-dessous:


![ligne de foulée](img/foulee.png){width=70%}

??? warning "**La réglementation française**"

     La réglementation française stipule que les marches courantes ont 17 de hauteur, mais ne doivent plus excéder 16  pour les lieux accueillant du<br>
     public et 28  de giron minimum (France : décrets n^o^ 2006-1657 et 1658 arrêté du du 15 janvier 2007)<br><br>

!!! info "**4 -- Calcul du giron**"   
     
     Diviser la hauteur d'escalier par 160 mm : hauteur de marche standard entre 160 et 180 mm <br>

     Pour un escalier de **2680 mm de hauteur** le nombre de marche est de
     **2680 / 160 = 16,75**<br>

    On arrondi à **16 **marches.<br>

    La hauteur de marche est donc 2680 / 16 = **167,50**<br>

    La longueur de la ligne de foulée dans notre cas est de :<br>

    2900 +600 + ((450 X 2 X π) ÷ 4 )) = **4206,86**<br>

   

    <p style="color:green">((450 X 2 X π) ÷ 4 étant la longueur du quart de l'arc de cercle ((Rayon x 2 x Pi) ÷ 4)).</p>

     <br><br>

    Giron = Longueur ligne de foulée ÷ nombre de marches -1<br>

    **280,46** = 4206,86 ÷ 15 <br>

    *La formule de Blondel* pour vérifier que l'escalier soit confortable.<br>



    M= (2 x h) + G   (2 X 167,50) + 280,46=**615** 
    M est le module (ou le pas) et doit être compris entre 600< **615**>640. h la hauteur de la marche G giron <br>

!!! info "**5 -- Tracer des longueurs de giron sur la foulée.**"
    === "ÉTAPE 1"
        A - Avec le compas reporter sur la ligne de foulée les longueurs du giron 280,46<br>
        ![étape 1](img/Et-1.png){ width=80% }

        

    === "ÉTAPE 2"
        B – Tracer les marches jusqu’au balancement des marches et la diagonale M .
        ![étape 2](img/Et-2.png){ width=80% }
     


!!! info "**6 -- Tracer le balencement de l 'escalier.**"
    === "ÉTAPE 3"
        
        ![étape 3](img/Et-3.png){ width=80% }

    === "ÉTAPE 4"
        
        ![étape 4](img/Et-4.png){ width=80% }
     

    === "ÉTAPE 5"
 
        ![étape 5](img/Et-5.png){ width=80% }
     



