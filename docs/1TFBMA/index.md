---
author: Alègre André
title: 🏡 1TFBMA
---
## Bac pro technicien de fabrication bois et matériaux associés (TFBMA)


!!! info "Objectifs de la formation"

    Le bac pro technicien de fabrication bois et matériaux associés forme à la production de petites ou moyennes séries d'ouvrages en bois et matériaux associés dans les entreprises de menuiserie et d'ameublement.

    L'élève apprend à préparer la fabrication d'ouvrages de menuiserie et d'ameublement. Il acquiert les techniques nécessaires pour fabriquer et conditionner les ouvrages, suivre et contrôler la fabrication, et participer à la maintenance des équipements. La formation lui donne des compétences pour prendre part aux activités d'organisation et maîtriser les techniques de produits ou composants tels que les meubles meublants, les mobiliers d'agencement, les menuiseries extérieures et intérieures, les charpentes industrielles et éléments en bois lamellés-collés.

    la durée totale des PFMP ( (période de formation en milieu professionnel) obligatoires pour l’examen, sur les trois années de formation, est de 20 semaines


Lycée professionnel de<br>
**_l’ameublement_**
Rue André-Charles BOULLE<br>
BP 53<br>
31250 REVEL<br>
Tél : 05 61 83 57 49<br>
Courriel : 0310088c@ac-toulouse.fr [Courriel](mailto:0310088c@ac-toulouse.fr) <br>

 Lien vers l ENT [Le lycée](https://ameublement-revel.mon-ent-occitanie.fr){:target="_blank" }. 