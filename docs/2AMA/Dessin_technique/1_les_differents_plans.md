---
author: Alègre André
title: Les différents types de plans
---

!!! info " **I. Les croquis :**"

    C'est un dessin réalisé à main levée, sans recherche de détails mais en essayant de garder les proportions.

    ![Alt text](../images/croquis1.jpg)




