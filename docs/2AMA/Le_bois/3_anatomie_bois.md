---
author: Alègre André
title: Anatomie du bois
---

!!! info "ANATOMIE DU BOIS"

    ![arbre](../images/anatomie-arbre.png)

    Ecorce : Couche protectrice comprenant le liber et le cambium.
    **Le liber :** Tissu qui conduit la sève élaborée<br>
    **Le cambium :** Tissu en formation (écorce)<br>
    **L’aubier :** Bois parfait en formation (nom duraminisé)<br>
    **Le bois parfait ou duramen :** c’est la partie « utile » du bois.<br>
    **La moelle :** c’est le cœur de l’arbre.<br>

    
!!!  example " Croissance et vaisseaux conducteurs des arbres"  

![arbre](https://youtu.be/ACSzcMSz9Qs){:target="_blank" }
    
    