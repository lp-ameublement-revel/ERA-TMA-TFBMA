---
author: Alègre André
title: CAO
---
!!! remarque "TopSolidWood"
     En CAO nous utilisons le logiciel TopsolidWood pour dessiner en 3D les agencements et pouvoir ensuite imprimer les plans.<br>
     Ci dessous une présentation sommaire et ensuite des exercices: <br>

!!! note "Présentation du Logiciel TopSolidWood"

    <div class="centre">
    <iframe 
    src="a_telecharger/presentation.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>


!!! note "Exercice 1: Modélisation d'une Lampe"

    <div class="centre">
    <iframe 
    src="a_telecharger/Lampe.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>


  [Exercice1 télécharger](https://nuage03.apps.education.fr/index.php/s/Ep2LazqAy8GYebg){ .md-button target="_blank" rel="noopener" }


!!! note "Exercice 1: Modélisation d'une Lampe"

   




