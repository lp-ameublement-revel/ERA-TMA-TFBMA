﻿---
author: Alègre André
title: 🏡 Bac Professionnel
---
![logo LP Revel](images/Logo.png){ width=30% }

<h1> 2AMA 2 nde Famille des métiers AMA</h1>>
  
!!! info "Objectifs de la formation:"
    L’organisation de la classe de 2nde famille de métiers permet<br>
    d’acquérir les premières compétences professionnelles utiles dans un secteur professionnel :<br>

    - de l’étude d’agencement <br>
    - de la fabrication sérielle ou unitaire de mobilier et de menuiserie.<br>
    Cette classe permet d’affirmer progressivement son choix d’orientation.


!!! info "Les 3 BAC Professionnels:"
    La 2nde famille de métiers de l’agencement, de la menuiserie et de
    l’ameublement regroupe trois Bac professionnels. <br>
    Des compétences pratiques (gestes professionnels) et des connaissances technologiques
    relatives aux trois Bac professionnels.<br> 
    seront dispensées sur la classe de seconde.<br>
    **La poursuite dans les formations est possible au sein de notre établissement :**



|Baccalauréat professionnel <br> Étude et Réalisation d’Agencement (E.R.A)| ![check](images/check.png)| Il peut accueillir 15 élèves |
|:---|:---:|:---:|
|**Baccalauréat professionnel <br>Technicien Menuisier Agenceur (T.M.A.)**| ![check](images/check.png)  | **Il peut accueillir 12 élèves**| 
|**Baccalauréat professionnel<br> Technicien de fabrication bois et matériaux associés (T.F.B.M.A.)**|![check](images/check.png)|**Il peut    accueillir 12 élèves**| 


!!! info "Découvrir:"
     Comment découvrir les autres formations de la famille de métiers?<br>
     Par des immersions dans l'établissements (mini-stages).<br>
     Lors des périodes de formation en milieu professionnel (PFMP), d’une durée totale de six semaines.


!!! info "Orientation:"
    Comment s’effectue l’orientation vers l’une des trois spécialités en 1ère?<br>
    Au fil de l’année le positionnement de chaque élève se précise.<br>
    L’orientation s’effectue selon le choix de l’élève, ses résultats et les places disponibles.

!!! example "quelques réalisations"

    === "Table basse"
        Table basse en hêtre

        ![nom image](images/table.png){ width=20% }

    === "Étagère Tétris"
        Étagère en forme du jeux de Tétris<br> 
        en Contre Plaqué et fond enstratifié de couleur

        ![nom image](images/jeux.png){ width=20% }

    === "Tabouret pliant"
        Tabouret pliant en hêtre

        ![nom image](images/tabouret.png){ width=20% }


Lycée professionnel de<br>
**_l’ameublement_**
Rue André-Charles BOULLE<br>
BP 53<br>
31250 REVEL<br>
Tél : 05 61 83 57 49<br>
Courriel : 0310088c@ac-toulouse.fr [Courriel](mailto:0310088c@ac-toulouse.fr) <br>

 Lien vers l ENT [Le lycée](https://ameublement-revel.mon-ent-occitanie.fr){:target="_blank" }. 





