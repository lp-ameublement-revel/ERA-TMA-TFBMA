---
author: Alègre André
title: Les plans
---


**S2 La communication technique** <br>
    S2.1 - Les systèmes de représentation.<br>
    S2.2 - Les documents techniques

**Les objectifs :** <br>
- Connaître les formats de papiers.<br>
- Le dessin par projection orthogonale.<br>
- Être capable de positionner les vues d'un objet simple.<br>
- Repérer et nommer les différentes vues.<br>


!!! info "1 - Les formats de papier :"
    Il existe 5 formats de papier utilisés en
    dessin industriel. Dans l'ordre croissant A4, A3, A2, A1 et A0 .<br>
    Pour savoir les dimensions du format au dessus, on multiplie par 2 le côté le
    plus petit et on conserve le plus grand.<br>

!!! example " Exemple :"
     A4 = 210 mm X 297 mm donc A3 = 420 mm *( 210 x 2) *X 297 mm

![format papier](../images/format_papier.png)

!!! info " 2 - Le dessin par projection orthogonale :"

    Elle a pour but de simplifier la réalisation d'un dessin. En effet, on
    représente le dessin vu suivant l'observation d'un dessin 3D, en 2D
    (deux dimensions).

!!! example " Exemple :"
    Considérons la pièce ci-dessous dans une boite cubique. <br>
    On projette chaque face suivant les six directions : haut, bas, gauche,
    droite, derrière et face pour observer leur représentation.

![Projection orthogonale](../images/projection_orthogonale.png) 

La vue la plus représentative de la pièce sera choisie comme vue de
face.<br> Le dessinateur sélectionnera parmi les cinq autres vues possibles,
celles qui montrent le mieux les formes et les contours. <br>Prenons comme
vue de face la vue suivant le sens de la flèche A.

La préférence ira aux vues ayant le moins de contours cachés ou de
traits interrompus. Les vues non nécessaires seront éliminées.

## 3 - Mise en plan :

![mise en plan](../images/mise-en-plan.png) 

### Disposition des vues 

Les vues sont nommées suivant la position de l'observateur par rapport à
la pièce.

<table>
<tr>
    <td style="border:1px solid; font-weight:bold;">Position de<br> l'observation</td>
    <td style="border:1px solid; font-weight:bold;">Dénomination <br> de la vue</td>
    <td style="border:1px solid; font-weight:bold;">Position de la vue<br>par rapport à la vue <br>de face</td>
    <td style="border:1px solid; font-weight:bold;">Repère de la vue</td>
    
</tr>
<tr>
    <td style="border:1px solid; font-weight:bold;">A</td>
    <td style="border:1px solid; font-weight:bold;">Vue de face</td>
    <td style="border:1px solid; font-weight:bold;"></td>
    <td style="border:1px solid; font-weight:bold;">1</td>
 </tr>
<tr>
    <td style="border:1px solid; font-weight:bold;">B</td>
    <td style="border:1px solid; font-weight:bold;">Vue de gauche</td>
    <td style="border:1px solid; font-weight:bold;">À droite</td>
    <td style="border:1px solid; font-weight:bold;">5</td>
</tr>
<tr>
    <td style="border:1px solid; font-weight:bold;">C</td>
    <td style="border:1px solid; font-weight:bold;">Vue de dessous</td>
    <td style="border:1px solid; font-weight:bold;">Au dessus</td>
    <td style="border:1px solid; font-weight:bold;">4</td>
<tr>
    <td style="border:1px solid; font-weight:bold;">D</td>
    <td style="border:1px solid; font-weight:bold;">Vue de dessus</td>
    <td style="border:1px solid; font-weight:bold;">Au dessous</td>
    <td style="border:1px solid; font-weight:bold;">2</td>
</tr>
<tr>
    <td style="border:1px solid; font-weight:bold;">E</td>
    <td style="border:1px solid; font-weight:bold;">Vue de droite</td>
    <td style="border:1px solid; font-weight:bold;">À gauche</td>
    <td style="border:1px solid; font-weight:bold;">3</td>
</tr>
<tr>
    <td style="border:1px solid; font-weight:bold;">F</td>
    <td style="border:1px solid; font-weight:bold;">Vue de derrière</td>
    <td style="border:1px solid; font-weight:bold;"></td>
    <td style="border:1px solid; font-weight:bold;">6</td>
</tr>
</table>

[Télécharger le cours](../a_telecharger/les_plans-AA.pdf){ .md-button target="_blank" rel="noopener" }
