---
author: Alègre André
title: L'arêtier
---
*S2 La communication technique** <br>
    S2.1 - Les systèmes de représentation.<br>
    S2.2 - Les documents techniques

**Les objectifs :** <br>
- Le dessin par projection orthogonale.<br>
- Tracer la vraie grandeur d'arête : VGA <br>
- Tracer l angle de corroyage.<br>
- Repérer et nommer les différentes vues.<br>



!!! info " Définition de L arêtier:"

     En termes professionnels, l’arêtier désigne une ligne saillante (arête) de rencontre entre deux versants d'un solide<br>
     (exemple: deux pans de charpente, pétrin de boulanger, deux faces de commode. ect..).

!!! example "Arêtier"

    === "Charpente"
        ![Arêtier Pyramide](images/charpente.png){ width=30% }

        

    === "Pétrin de boulanger"
        ![Arêtier Pyramide](images/petrin.png){ width=25% }


    === "Commode LouisXV"
        

        ![Arêtier Pyramide](images/commode-louisXV.png){ width=25% }


!!! example "Pyramide régulière" 
    Tracé et de modélisation sans notions d'épaisseurs des matériaux (tôle par exple) d'une pyramide régulière.


!!! info "Tracer la vraie grandeur de l arête : la VGA"
     Matériel nécessaire:
     Crayon , gomme, règle graduée, équerre, compas, paire de ciseaux et une feuille A4.<br>
     Prenons une pyramide carré 50 mm X 50 mm et de hauteur 60 mm .<br>
     Suivre les étapes ci dessous.  



!!! example "Pyramide à base carré"

    === "ÉTAPE 1"
         Tracer la vue de dessus et la vue de face

        ![étape 1](tracer_1/etape-1.png){ width=50% }

        

    === "ÉTAPE 2"
         Repporter à l aide d'un compas l'arête OA sur la ligne de sol.

        ![étape 2](tracer_1/etape-2.png){ width=50% }

    === "ÉTAPE 3"
         On trouve la Vraie Gradeur de l Arête :VGA.
         
        ![étape 3](tracer_1/etape-3.png){ width=50% }

    === "ÉTAPE 4"
         Tracer cercle de rayon *JD* et tracer les 4 côtés de la pyramide.

        ![étape 4](tracer_1/etape-44.png){ width=50% }

    === "ÉTAPE 5"
          Découper la pyramide.

        ![étape 5](tracer_1/etape-4-2.png){ width=50% }
        
    === "ÉTAPE 6"
          Plier la pyramide.

        ![étape 6](tracer_1/etape-6.png){ width=30% }  
        ![étape 7](tracer_1/etape-7.png){ width=30% }  


!!! info "Tracer l'angle de corroyage"
    L'angle de corroyage ou l'angle driède (en géométrie) est l'angle qui reliera deux côtés d'une pyramide.

    ![angle de corroyage](angle_corroyage/angle-fausse-equerre.png)

    Ci dessous en vert l'angle de corroyage:~/docs/TTMA/GAretier/angle_corroyage/img_VGA_aretier
      
    ![Arêtier Pyramide](images/aretier-pyramide.png)


!!! note "Reprenons notre pyramide."
    
    Aprés avoir trouvé la VGA , nous allons rechercher l'angle de corroyage.<br>
    === "ÉTAPE 1"
         Tracer une ligne passant par **JI** et une ligne partant de **D** suivant la base de la pyramide.<br>
         on obtient le point **O**.

        ![étape 1](img_VGA_aretier/etape-1_VGA.png){ width=50% }

    === "ÉTAPE 2"
         Tracer une horizontale passant par le sommet de la pyramide.<br> 
         Une verticale au point **O**. <br>
         L'intersection des 2 lignes donne le point **A**

        ![étape 1](img_VGA_aretier/etape-2_VGA.png){ width=50% }

    === "ÉTAPE 3"
         Tracer de centre **O** et de rayon **OA** et on obtient à l'intersection avec la continuation de **JI** le point **B** <br> 

        ![étape 1](img_VGA_aretier/etape-3_VGA.png){ width=50% }
        
    === "ÉTAPE 4"
         Une perpendiculaire à la ligne **JI** passant par le point **O** . <br>
         Une parallèle à **JI** passant par le point **D** et on obtient au croisement avec la perpendiculaire précédemment tracer le point **C**<br>
         La ligne **BC** est la VGA (Vrai Grandeur de l'Arête) <br>
         

        ![étape 1](img_VGA_aretier/etape-4_VGA.png){ width=50% }

    
    === "ÉTAPE 5"
         Tracer  la perpendiculaire à **AB** passant par **O**, et on obtient le point **E**<br>
         
        ![étape 2](img_VGA_aretier/et-2_aretier.png){ width=50% }
        
    === "ÉTAPE 6"
         On trace un cercle de centre **O** et  de rayon **OE**., et on obtient le point **F**<br>
         Une parallèle à la droite **CD** passant par **F**, et on obtient le point **G**
         
        ![étape 3](img_VGA_aretier/et-3_aretier.png){ width=50% }
       
    === "ÉTAPE 7"
         On rejoint les points **IG** et **JG**<br>
         On obtient un angle qui est l'angle de corroyage.
         
        ![étape 3](img_VGA_aretier/et-4_aretier.png){ width=50% }

    