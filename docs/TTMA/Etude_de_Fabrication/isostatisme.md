---
author: Alègre André
title: Isostatisme
---
**Compétences :** C2.5 Établir des documents de suivi<br>
**Savoirs associés :** S2.2 Les documents technique<br><br>

!!! info "**Définition**"
    ![](images/TR_XYZ.png){ width=30%; align=right }
     Le mouvement d’un solide dans l’espace peut se décrire suivant la combinaison de 3 translations et de 3 rotations<br>
     par rapport à une base orthogonale.<br><br>
     Ces 6 mouvements représentent les 6 degrés de liberté du solide. Pour immobiliser un solide dans l’espace, il<br>
     suffit de supprimer ces 6 degrés de liberté.<br><br>
     En fabrication, l’isostatisme, c’est l’étude de la suppression des degrés d’un solide. Il est en effet préférable<br> 
     que la pièce soit bien mise en place pendant les opérations d’usinage.<br>


??? warning "Attention"
    Il ne faut pas confondre la mise en position (qui correspond à l’isostatisme) et le maintien de la pièce par un serrage.
       

![](images/TZ.png){width=40%} ![](images/TY.png){width40%}![](images/TX.png){width40%}

![](images/RZ.png){width40%} ![](images/RY.png){width40%}![](images/RX.png){width40%}



!!! info "**Élimination des degrés de liberté :**"

     Lorqu'on élimine les 6 degrés de liberté, le solide devient immobile. <br> <br>

     
      **3 appuis non alignés appelés 1, 2, 3** (un plan)<br>
      permettent de supprimer **1 translation** et **2 rotations.**<br>
       ![](images/Appuis_1_2_3.png){ width=30% }


    . 
      **2 appuis linéaires appelés 4, 5** (un guide)<br>
      permettent de supprimer **1 translation** et **1 rotation.**<br>
      ![](images/Appuis_4_5.png){ width=40% }

    . 
      **1 appui fixe appelé 6 **(butée) permet de supprimer **1 translation**.<br>
      ![](images/Appuis_6.png){ width=40% }

     Éliminer les 6 degrés de liberté d’un solide dans un repère O x y z s’appelle encore effectuer **sa mise en position.**<br>
     Si les 6 degrés de liberté sont supprimés on dit que la mise en position est **isostatique**. <br>
     Si plus de 6 appuis ponctuels apparaissent, la mise en position est **hyperstatique**.


!!! info "**Mise en position isostatique d’une pièce**"    
     **1**: Mouvement possible             **0**: Pas de mouvement possible<br><br>

    ![](images/Degre-0-T.png){ width=60% }<br>

       
    ![](images/Degre-3-T.png){ width=60% }

       
    ![](images/Degre-5-T.png){ width=60% }

       
    ![](images/Degre-6-T.png){ width=60% }
      