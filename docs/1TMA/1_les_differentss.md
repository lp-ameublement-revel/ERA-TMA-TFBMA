---
author: Alègre André
title: Les
---

## I. Les croquis :

C'est un dessin réalisé à main levée, sans recherche de détails mais en essayant de garder les proportions.

![Alt text](images/croquis1.jpg)
### 1. Sous paragraphe 1

Texte 1.1

### 2. Sous paragraphe 2

Texte 1.2

## II. Paragraphe 2 : Quelques formules

Utiliser LaTeX

### 1. En maths

Une suite : 

$$
\left\{
        \begin{array}{ll}
            u_0 = 3 \\
            u_{n+1} = 5 \times u_n+2\\
        \end{array}
\right.
$$



