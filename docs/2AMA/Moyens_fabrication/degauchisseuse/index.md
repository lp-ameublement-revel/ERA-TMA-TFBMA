---
author: Alègre André
title: Dégauchisseuse
---

!!! info "Définition du verbe dégauchir**"
    Dégauchire permet à partir de pièces de bois brutes, de réaliser deux plans de références (SR1 et SR2) généralement d'équerre (90°).

    ![](images/Sr1_Sr2.png){ width=70%; align=left}
    ![](images/degau.jpg){ width=70%; align=left}


!!! info "**FONCTION ET MOUVEMENTS DES ORGANES:**"

    L'OUTIL : lame en acier allié, ou avec mise rapportée au carbure de tungstène,<br>
    assurant la coupe de la matière. Longueur : de 260 à 600 mm.<br>

    Angle de taillant : β : 35 à 45°. Largeur 25 à 40 mm, épaisseur : 2,5 à 3,5 mm<br>
    ![](images/angle_taillant.png){ width=20%}<br><br>

    LE PORTE OUTIL :<br>
     a) cylindre assurant le mouvement de coupe et supportant 2, 3 ou 4 fers. <br>
     b) organes de serrage assurant la mise en position et le maintien des fers. 
      ![](images/Cylindre_coupe.jpg){ width=60%}<br><br>




     LE PORTE OUTIL :<br>
     a) cylindre assurant le mouvement de coupe et supportant 2, 3 ou 4 fers. <br>
     b) organes de serrage assurant la mise en position et le maintien des fers. <br><br>
     LE BÂTI :  ossature supportant tous les organes de la machine.<br><br>

     LA TABLE D'ENTRÉE : reçoit la pièce à l'état brute. Détermine la hauteur de passe (maxi 15 mm)<br><br>


     LA TABLE DE SORTIE :   réceptionne la pièce usinée. Le plan de cette table doit être parfaitement tangent au cylindre de coupe.


     LA TABLE DE SORTIE :   réceptionne la pièce usinée. Le plan de cette table doit être parfaitement tangent au cylindre de coupe.
      LE GUIDE : Sert de support à la face (SR1) de la pièce pour le dressage du chant (SR2)
		En général à 90 ° mais peut s’incliner à 135 °.<br><br>

!!! warning "**Organes de protection**"
    Protection du cylindre de coupe (protecteur à pont). Toutes parties en mouvement doivent être sous carter. <br>

     ![](images/protect_SR1.jpg){ width=60%}
     ![](images/protect_SR2.jpg){ width=60%}<br><br>

     ORGANES DE RÉGLAGE :<br>

     a) Réglage de la table d'entrée. <br>

     b) Réglage de la table de sortie , surtout lorsqu’on change les fers.<br>

     c) Réglage du guide pièce.<br>
