---
author: Alègre André
title: Casse tête
---

**S2 La communication technique** <br>
    S2.1 - Les systèmes de représentation.<br>
    S2.2 - Les documents techniques.

**Les objectifs :** <br>
- Lecture de plans.<br>
- Traçage sur le bois à l aide de trusquin, équerre et crayon.<br>
- Utiliser les outils à main (scie et ciseaux)<br>
- Finition et montage. <br>


!!! example "Les plans"

    === "Casse tête"
        Perspective

        ![nom image](images/plan1.png){ width=60% }

    === "Les 6 pièces"
       

        ![nom image](images/perspective-pieces.png){ width=60% }

    === "Pièces A et B"
        Plans Pièces A et B

        ![nom image](images/plan-pieces-A-B.png){ width=60% }

    === "Pièces C et D"
        Plans Pièces C et D

        ![nom image](images/plan-pieces-C-D.png){ width=60% }

    === "Pièces E et F"
        Plans Pièces E et F

        ![nom image](images/plan-pieces-E-F.png){ width=60% }


🌐 Télécharger les plans du Casse tête:

Fichier `file_telecharger` : [Clic droit, puis "Enregistrer la cible du lien sous"](a_telecharger/plan-casse-tete-20-20.zip)
