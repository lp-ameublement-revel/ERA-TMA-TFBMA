---
author: Alègre André
title: Contrat de phase
---


!!! info "**Définition**"
        
    Document de référence établi par le bureau des méthodes destiné à l'opérateur de machine outil.<br> 
    Ce document définit entre autre les cotes de fabrication de la pièce et comporte tous les renseignements utiles pour la réalisation d'une phase déterminée.<br>

    <span style="color:green"> *Bureau des méthodes* </span> (B.M. ou B.d.M.).<br>
    Secteur d'une entreprise où l'on prépare et organise le travail à partir du dessin de définition élaboré en bureau d'étude, en vue de la fabrication. <br><br>
    
    <span style="color:green">*Bureau d'étude*</span> (B.E. ou B.d.E.) <br>
    Secteur d'une entreprise où l'on conçoit et définit les produits .<br><br>

    <span style="color:green">*Machine-outil conventionnelle*</span> (MO) :<br> 
    Type de machine-outil où les commandes sont contrôlées par l'opérateur.<br><br> 

    <span style="color:green">*Machine-outil à commandes assistées*</span> (MOCA) : <br>
    Type de machine-outil permettant à l'opérateur de choisir entre :<br> 
    le mode manuel. <br>
    le mode semi-automatique. <br> le mode automatique.<br> 
    
    <span style="color:green">*Machine-outil à commandes numériques*</span> (MOCN) (CN) : 
    Type de machine-outil où les commandes sont contrôlées par un programme.<br> 
    Sur chaque axe de déplacement est placé un capteur numérique (codeur) qui rend compte de la position de l'outil.<br>
    L'opérateur n'intervient qu'au moment du réglage et de la mise au point du programme.<br><br>

    <span style="color:green">*Cotes de fabrication*</span> <br>

    Ce sont des cotes, calculées à partir des spécifications du dessin de définition, qui apparaissent sur le contrat de phase.<br>
    Elles doivent être respectées et sont contrôlées lors de l'usinage.<br><br> 

    <span style="color:green">*Géométrie de l'outil :*</span> <br>

    Ensemble des faces, des arêtes, des angles qui caractérisent un outil de coupe pour une situation d'usinage donnée.<br><br> 

    
    <span style="color:blue">*Éléments de coupe :*</span>  
 


    |Symbole | Dénomination| Unité||
    |:---|:---:|:---:|:---:|
    | <span style="color:red"><b>Vc</b></span>|Vitesse de coupe |**m/s**| |
    | <span style="color:red"><b>n</b></span>|Fréquence de rotation|**tr/min**| |
    <span style="color:red"><b>Z</b></span>|Nombre d'arêtes tranchantes|| |
    <span style="color:red"><b>Vf</b></span>|Vitesse d'avance|**m/min**| |
    <span style="color:red"><b>f</b></span>|Pas principal d'usinage ou (avance par tour)|**mm**| |
    <span style="color:red"><b>fz</b></span>|Pas secondaire d'usinage|**mm**| |
    <span style="color:red"><b>![check](images/ap.png)</span>|Profondeur de passe|**mm**| |
    <span style="color:red"><b>![check](images/de.png)</span>|Diamètre extérieur du cylindre de coupe (l'outil)|**mm**| |
    <span style="color:red"><b>![check](images/em.png)</span>|Épaisseur moyenne du copeau|**mm**| |
  
   
!!! warning "Les lois d'usinage:"

    Savoir appliquer les lois d’usinage  (fréquence de rotation, vitesse de coupe, avance du matériau) en fonction de l’outil (nature, diamètre, arête tranchante);<br> 
    est importante pour travailler en toute sécurité. Mais nous savons aussi que la finition d’une oeuvre <br>dépend non seulement du raclage, papier de verre, etc. mais également d’un bon réglage des machines, <br>
    de la fréquence de rotation, de l’avance de la pièce afin d’éviter une perte de temps aux techniques de finition.<br><br>

    ![](images/pas_pri_secon.png){width=60%}
!!! info "**Usinage du bois en sécurité**"
    L’abaque permet en sachant le diamètre de l outils et la nature des arêtes tranchantes de déterminer la fréquence de rotation. <br><br>
    ![](images/abaque.png)<br><br><br>


!!! info "ÉTUDE DES PARAMÈTRES DE COUPE"   
    **Vitesse d’Avance** : **Vf** en mètre par minute m/min<br>
    c’est la vitesse du déplacement entre la pièce et l’outil. Cela correspond à la<br>
    longueur d’usinage réalisé pendant la durée d’une minute. <br>
     Caractéristiques des paramètres :<br> 
    **fz** = Pas d’usinage exprimé en millimètre (mm) <br>
    **Z** = Nombre d’arêtes tranchantes (dépend de l’outil)<br> 
    **n**= Fréquence de rotation exprimée en tour par minute (tr/min)<br>
   
    ![](images/Vf_2.png)<br><br><br>

    **Vitesse de coupe** : **Vc** en mètre par seconde m/s<br>

    C’est la distance parcourue par une arête tranchante en une seconde. <br>
    **π** = 3,14<br>
    **de** = Diamètre de l'outil en millimètre (mm)<br> 
    **n**= Fréquence de rotation exprimée en tour par minute (tr/min)<br>  
    **60** = nombre de secondes comprises dans une minute.<br>
    ![](images/Vc.png)<br><br><br>


    **Fréquence de rotation** : **n** en tour par minute tr/min<br>

    C’est le nombre de tours qu’effectue une arête tranchante en une minute. <br>
     Caractéristiques des paramètres :<br> 
    **π** = 3,14<br>
    **de** = Diamètre de l'outils en millimètre (mm)<br> 
    **Vc**= Vitesse de coupe exprimée en mètre par seconde (m/s) <br>  
    **60** = nombre de secondes comprises dans une minute.<br>
    ![](images/n.png)<br><br><br>



    **Pas d'usinage** : **fz** en millimètre mm<br>
    C'est la trace laissée par le passage **d’une arête tranchante** de l’outil pour un tour de rotation de l’outil<br>
    **Vf** = Vitesse d’avance exprimée en mètre par minute (m/min)<br>
    **Z** = Nombre d’arêtes tranchantes (dépend de l’outil)<br>
    **n** = Fréquence de rotation exprimée en tour par minute (tr/min)<br>
    ![](images/fz.png)<br><br>

    **Profondeur de passe**: **ap** profondeur de passe en millimètre mm . <br>
    C’est la profondeur de passe ou pénétration de l’arête tranchante dans la matière, elle est
    exprimée en mm.<br>


!!! info "<span style="color:#1E90FF">**La symbolisation des appuis et des maintiens en position**"
    ![](images/symboles.png)<br><br><br>


 
!!! info "<span style="color:#1E90FF">**Abréviations conventionnelle des machines outils**"     
    
    |Familles|Nom|Type|Sym_M_outil|Désignations|
    |:---:|:---|:---|:---:|:---|
    |SR|Scie à ruban|Tous types|SR|Sciage à ruban|
    |SC|Scies circulaires|à tronçonner|SCT|Tronçonnage|
    |||à déligner|SCD|Délignage|
    |DE|Dégauchisseuse|à une face|DE|Dégauchissage|
    |RA|Raboteuse|à une face|RA|Rabotage|
    |MO|Mortaiseuses|à mèches|MOM|Mortaisage|
    |||à bédane vibrant|MOV||
    |TE|Tenonneuses|à dérouleurs|TED|Tenonnage|
    |TO|Toupies|à arbre vertical|TOV|Profilage|
    |||à arbre inclinable|TOI||
    |PE|Perceuses|à broche unique|PE|Perçage|
    |||multibroches|PEM||
    |DE|Défonceuse|à commande numérique|DFCN|Défonçage|
    |PO|Ponceuse|à bande large|POL|Ponçage|
    |CD|Cadreuse|semi-verticale|CDSV|Assemblage|
    |Q4|Multi-opératrices|4 faces|Q4M|Corroyage|

!!! info "<span style="color:#1E90FF">**Exemple de contrat de phase**" 
     ![](images/Contrat_Phase.png)<br><br><br>