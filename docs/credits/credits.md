---
author: Alègre
title: 👏 Crédits
---

Le site est hébergé par la forge des communs numériques éducatifs <a href="https://docs.forge.apps.education.fr/">
Centre de documentation</span></a>

![AEIF](../assets/images/logo_aeif_300.png){width=7%}    
Le modèle du site a été créé par l'  [Association des enseignantes et enseignants d'informatique de France](https://aeif.fr/index.php/category/non-classe/){target="_blank"}.  





