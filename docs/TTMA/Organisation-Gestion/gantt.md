---
author: Alègre André
title: Le GANTT
---
**S9  L’organisation et la gestion de fabrication et de chantier** <br>
    9.12 - La chronologie des étapes

!!! info "Un peu d'histoire"    

     <b> Le diagramme de Gantt a été inventé par Henry Gantt en 1910. </b> <br>
     C’est un outil graphique permettant de représenter la progression d’un projet, d’en visualiser l’évolution et la durée.<br>
     Il consiste à déterminer la meilleure manière de positionner les différentes tâches d'un projet à exécuter,<br>
     sur une période déterminée, en fonction :<br>
      - l’enchaînement des tâches.<br>
      - le délai de chaque tâche.<br>
      - les dépendances entre les tâches.<br>
      - le chevauchement entre les activités.<br>

!!! example " Exemple :"
    <b> Présentation de la technique du diagramme de GANTT.</b> <br>

    Une entreprise de menuiserie doit réceptionner une livraison de panneaux de contre-plaqué.<br>
    20 panneaux doivent être décharger : 8 qui partiront sur le centre d’usinage et les 12 autres  à la mise en stock.<br>        Une validation informatique est réalisé à la fin de toutes ces étapes.<br>

    <ins> -Les étapes et leur durée :</ins><br>
     - Déchargement 1 minute par panneau<br>
     - Acheminement transit  2 minutes pour 4 panneaux<br>
     - Mise en stock 1 minute pour 2 panneaux<br>
     - Validation informatique 4 minutes pour la livraison<br>

    <b> Étape 1 : Codifier les étapes</b> <br>
    On énumère les différentes étapes en les énumérant par une lettre et en indiquant leur durée.<br>
    Déchargement du véhicule <b>A</b> <br>
    Acheminement vers le stock ou le centre d’usinage CN <b>B</b> <br>
    Mise en stock <b>C</b> <br>
    Validation informatique<b> D</b> <br><br>

    <b>Étape 2 : Calculer la durée de chaque étape</b><br>

    |Étapes| Tâches| Durée|
    |:---|:---:|:---:|
    |Déchargement du véhicule |Tâche <b>A</b>|20 min|
    |Acheminement > stock >centre d’usinage |Tâche <b>B</b>|10 min|
    |Mise en stock |Tâche <b>C</b>|6 min|
    |Validation informatique|Tâche <b>D</b>|4 min|
    <br><br>
    <b> Étape 3 : Déterminer les antériorités</b><br>  
    Antériorité : tâche qui précède immédiatement une autre tâche.</br>
    
     
    |Étapes| Tâches| Durée| Antériorités|
    |:---|:---:|:---:|:---:|
    |Déchargement du véhicule |Tâche A|20 min|-|
    |Acheminement > stock >centre d’usinage |Tâche B|10 min|<b>A</b>|
    |Mise en stock |Tâche C|6 min|<b>B</b>|
    |Validation informatique|Tâche D|4 min|<b>B C</b>|

    <b>Étape 4 : Représenter graphiquement les tâches et leur durée.</b><br>
    En abscisse le temps.<br>
    En ordonnée les différentes tâches.<br>
    On représente en bleu les tâches avec leur durée (exple A = 20 minutes)<br>

    ![](images/Tableau-duree.png){ width=60% }<br>

    On relie les tâches entre elle (trait rouge)<br>

    ![](images/Tableau-taches-reliées-rouge.png){ width=60% }<br>

    <b>Étape 5 : Repérer les marges « libres ».</b><br>

    Marge libre : marge qui ne pénalise pas la durée totale de l’activité.<br>

    ![](images/Tableau-marge-libre.png){ width=60% }<br>

    Nous pouvons prendre du retard sur la tâche C ; sans pénalisé la durée totale de l’activité.<br>

    <b>Étape 6 : Déterminer le chemin critique.</b><br>
    Chemin critique : chemin sans marche libre.<br>
    C’est à dire la liste des tâches ou il ne faut ps prendre du retard , sans allonger la durée totale. <br>
    <ins>Je pars de la dernière tâche</ins> ici la <b>D</b>.<br>
    
    ![](images/Tableau-chemin-critique.png){ width=60% }<br>

    <b>Étape 6 : Dimensionner les équipes.</b><br>
    Il n’y a jamais plus de  2 tâches en même temps , la <b>B</b> et la <b>C</b> .<br>
    Donc on aura besoin au maximum de 2 opérateurs.<br>

!!! info "Dans les entreprises, le diagramme de Gantt se traduit graphiquement :"
    > Soit sur un planning mural sur lequel on positionne des barres<br>
    cartonnées ou plastiques de couleurs et de longueurs différentes qui
    représentent les opérations à réaliser.<br>
    > Soit par un logiciel informatique qui simule un diagramme de Gantt
    à l'écran.<br>

    Dans une situation comme dans l'autre, le diagramme n'est pas un
    planning figé. Au contraire, il s'agit un planning dynamique qui doit faire<br>
    apparaître en temps réel les modifications d'opérations en dates et
    durées, liées aux impératifs de  replanification  et reprogrammation.<br>
    Le diagramme de Gantt n'est pas qu'un outil de résolution ponctuelle
    d'un problème. On le trouve très souvent dans les entreprises, utilisé dans<br>
    les ateliers comme outil de planning d’ordonnancement – lancement de
    la production quotidienne.<br>

[Télécharger le cours](../a_telecharger/le-diagramme-GANTT-1.pdf){ .md-button target="_blank" rel="noopener" }
