---
author: Alègre André
title: 🏡 Acceuil 1TMA
---

## Bac pro technicien menuisier-agenceur (TMA)



!!! info "Objectifs de la formation"

     Ce bac pro forme à la fabrication de différents ouvrages de menuiseries extérieures (fenêtres, volets, portails...), intérieures (portes, escaliers, parquets...), de menuiseries d'agencement (placards, rangements, dressings, rayonnages...), et d'aménagements de pièces (bureau, cuisine, salle de bains), de magasins, salles d'exposition, lieux de réunion...

     Les élèves apprennent toutes les étapes de réalisation d'un ouvrage de menuiserie, de sa préparation à sa fabrication, sa pose et son installation. Ils étudient les différents matériaux utilisés en menuiserie (bois, métalliques, en plaques, isolants...), les produits (de jointement, de fixation, de traitement, de finition...), et leurs propriétés (mécaniques, de résistance...). En technologie, ils travaillent les procédés de coupe (sciage, perçage...), l'usinage sur machines et les techniques de montage, d'assemblage, de placage et de finition (pose d'accessoires et quincailleries). Ils se familiarisent avec le contrôle qualité du produit fini, son conditionnement et son stockage. La formation aborde aussi l'entreprise et son environnement, ainsi que l'organisation et la gestion de fabrication sur chantier (délais, coûts de fabrication...).

     La durée totale des PFMP ( (période de formation en milieu professionnel) obligatoires pour l’examen, sur les trois années de formation, est de 20 semaines.


Lycée professionnel de<br>
**_l’ameublement_**
Rue André-Charles BOULLE<br>
BP 53<br>
31250 REVEL<br>
Tél : 05 61 83 57 49<br>
Courriel : 0310088c@ac-toulouse.fr [Courriel](mailto:0310088c@ac-toulouse.fr) <br>

 Lien vers l ENT [Le lycée](https://ameublement-revel.mon-ent-occitanie.fr){:target="_blank" }. 

  