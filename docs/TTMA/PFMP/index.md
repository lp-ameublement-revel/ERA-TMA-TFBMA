---
author: Alègre André
title: Pèriode PFMP 2024/2025
---

!!! danger "Pèriode PFMP 2024-2025"
    **Première période:** 30 septembre 2024 au 22 novembre 2024 (avec 2 semaines des vacances de Toussaint). <br><br>
    
    Et les élèves qui ne poursuivent pas les études en BTS. <br>
    **Deuxième période:** 26 mai 2025 au 20 juin 2025.<br>


    Calendrier PFMP:  [Ouvrir dans un nouvel onglet](../../a_telecharger/periode-PFMP.pdf){:target="_blank" }
