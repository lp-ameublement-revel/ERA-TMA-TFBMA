---
author: Alègre André
title: L'arbre
---
![format papier](../images/arbre-parties.jpg)

## 1 - Fût

>Partie non ramifiée du tronc de l’arbre, comprise entre la souche et la naissance des premières branches maîtresses.

## 2 - Branche maîtresse

>Ramification directement issue du tronc de l’arbre, qui se subdivise par la suite en rameaux et en ramilles.

## 3 - Ramille

>Ramification la plus fine d’un rameau de l’arbre.

## 4 - Houppier

>Partie de l’arbre située au-dessus du tronc, qui comprend la ramure et le feuillage.

## 5 - Cime

>Extrémité supérieure du houppier de l’arbre.

## 6 - Feuillage

>Ensemble des feuilles de l’arbre, spécialisées dans la captation de la lumière et la fonction de photosynthèse.

## 7 - Rameau

>Ramification d’une branche maîtresse d’un arbre.

## 8 - Ramure

>Ensemble des branches, rameaux et ramilles qui supportent les feuilles, les fleurs et les fruits de l’arbre.

## 9 -details Racine traçante
>Racine souvent fortement ramifiée poussant plus ou moins horizontalement, dans la couche arable du sol, humide et riche.


## 10 - Racine pivotante

>Première racine issue de la graine, qui s’enfonce verticalement dans le sol. Généralement peu ramifiée, son rôle principal est d’assurer la fixation de l’arbre.

## 11 - Radicelle
